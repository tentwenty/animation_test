<?php
defined('C5_EXECUTE') or die("Access Denied.");
$form_page = Page::getCollectionPathFromID(200);
?>

<?php $this->inc('elements/header.php'); ?>

<main data-scroll-container>
    <div data-scroll-section class='header-wrapper __border-bottom'>
        <div class='a-layout'>
            <div class='a-layout_item heading-title fade-in'>
                <a href="/index.php">Tentwenty</a>
                <!-- <button></button> -->
            </div>
            <div class='a-layout_item -right let-talk slide--left'>
                <a href="index.php<?php echo $form_page; ?>">Let's Talk</a>
            </div>
        </div>
    </div>

    <!-- <div data-router-wrapper> -->
    <?php
    View::element('system_errors', [
        'format' => 'block',
        'error' => isset($error) ? $error : null,
        'success' => isset($success) ? $success : null,
        'message' => isset($message) ? $message : null,
    ]);

    echo $innerContent;
    ?>
    <!-- </div> -->


</main>

<?php $this->inc('elements/footer.php'); ?>
<?php $this->inc('elements/scripts.php'); ?>