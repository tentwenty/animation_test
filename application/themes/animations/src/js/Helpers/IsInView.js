export default class IsInView {
  constructor(params) {}

  observe = (targets, callback) => {
    const inView = (target) => {
      const io = new IntersectionObserver((entries, observer) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            let element = $(entry.target);

            callback(true);
            observer.disconnect();
          }
        });
      });

      io.observe(target);
    };

    targets.forEach(inView);
  };
}
