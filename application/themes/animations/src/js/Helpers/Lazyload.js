import Lozad from "lozad";

export default class LozadLoad {
  constructor(params) {
    this.init();
  }

  init = () => {
    if (typeof $("main").data("scroll-container") === "undefined") {
      this.lozad();
    } else {
      this.customIntersectionLoad();
    }
  };

  lozad() {
    const observer = Lozad();
    observer.observe();
  }

  customIntersectionLoad = () => {
    const targets = document.querySelectorAll(".lozad");

    const lazyLoad = (target) => {
      const io = new IntersectionObserver((entries, observer) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            if (entry.target.tagName == "IMG") {
              const img = $(entry.target);

              const src = img.attr("data-src");

              img.attr("src", src);
            } else {
              const img = $(entry.target);

              const src = img.attr("data-src");

              img.css({ backgroundImage: `url(${src})` });
            }

            observer.disconnect();
          }
        });
      });

      io.observe(target);
    };

    targets.forEach(lazyLoad);
  };
}
