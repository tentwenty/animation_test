export default class SplitText {
  constructor(params) {
    this.init();
  }

  init = () => {
    $(".__split-text.p p").each((k, v) => {
      this._buildStructure(v);
    });
    $(".__split-text.h1 h1").each((k, v) => {
      this._buildStructure(v);
    });
    $(".__split-text.h2 h2").each((k, v) => {
      this._buildStructure(v);
    });
    $(".__split-text.h3 h3").each((k, v) => {
      this._buildStructure(v);
    });
    $(".__split-text.h4 h4").each((k, v) => {
      this._buildStructure(v);
    });
    $(".__split-text.h5 h5").each((k, v) => {
      this._buildStructure(v);
    });
    $(".__split-text.h6 h6").each((k, v) => {
      this._buildStructure(v);
    });
  };

  _buildStructure(text) {
    let parentEl = $(text);

    let str_array = parentEl.text().split(/(?!$)/u);
    let str_build = "";
    for (let i = 0; i < str_array.length; i++) {
      str_build += "<span>" + str_array[i] + "</span>";
    }
    parentEl.empty().append(str_build);
  }
}
