import Locomotive from "locomotive-scroll";

export default class SmoothScroll {
  constructor() {
    this.scroll;
    this.init();
  }
  init = () => {
    this.bindEvents();
  };
  bindEvents = () => {
    this.initScrollContainer();
    this.handleEditMode();
  };

  initScrollContainer = () => {
    this.scroll = new Locomotive({
      el: document.querySelector("[data-scroll-container]"),
      smooth: true,
    });
  };

  handleEditMode = () => {
    let editAreas = $(".ccm-area");
    if (editAreas.length) {
      this.scroll.destroy();
    }
  };
}
