import Navbar from "./components/Navbar";
import GsapAnimations from "./gsap/index";
export default class CoreAnimation {
  constructor() {
    this.init();
  }

  init = () => {
    this.bindEvents();
  };

  bindEvents = () => {
    new Navbar();
    new GsapAnimations();
  };
}
