import NavSlider from "../gsap/components/navbar";
export default class NavHelper {
  constructor() {
    this.isOpen = false;
    this.navbarSlider = new NavSlider();
    this.toggleNavbar();
  }
  toggleNavbar = () => {
    $("#hamburger-trigger").on("click", () => {
      if (this.isOpen) {
        this.close();
      } else {
        this.open();
      }
    });
  };
  open = () => {
    this.isOpen = true;
    $("div[data-nav-open]").each((k, v) => {
      let el = $(v);
      el.addClass("nav-open");
    });
    $(".open-navSlide-container").css({ display: "inline-flex" });
    this.navbarSlider.open();
  };
  close = () => {
    this.isOpen = false;
    $("div[data-nav-open]").each((k, v) => {
      let el = $(v);
      el.removeClass("nav-open");
    });
    this.navbarSlider.close();
    setTimeout(() => {
      $(".open-navSlide-container").css({ display: "none" });
    }, 1500);
  };
}
