import MasterTimeline from "../masterTimeline";

export default class NavSlideOpen {
  constructor(params) {
    this.masterTimeLine = new MasterTimeline();
    this.mtl = this.masterTimeLine.getMasterTimeline({});
    this.gsap = this.masterTimeLine.gsap;
  }

  play = ({ toggle }) => {
    this.mtl.add(toggle ? this.slideOpen() : this.slideClose());
    return this.mtl;
  };

  slideOpen = () => {
    let elName = ".open-navSlide";
    this.gsap.set(elName, { width: "0" });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(elName, {
      width: "100%",
      duration: 1.5,
      ease: "circ.inOut",
    });
    return tl;
  };

  slideClose = () => {
    let elName = ".open-navSlide";
    this.gsap.set(elName, { width: "100%" });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(elName, {
      width: "0",
      duration: 1.5,
      ease: "circ.inOut",
    });
    return tl;
  };
}
