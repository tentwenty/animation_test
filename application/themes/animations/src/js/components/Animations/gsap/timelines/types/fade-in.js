import MasterTimeline from "../masterTimeline";

export default class FadeIn {
  constructor(params) {
    this.masterTimeLine = new MasterTimeline();
    this.mtl = this.masterTimeLine.getMasterTimeline({});
    this.gsap = this.masterTimeLine.gsap;
  }

  play = () => {
    this.mtl.add(this.fadeIn());
    return this.mtl;
  };

  fadeIn = () => {
    let elName = ".fade-in";
    this.gsap.set(elName, { opacity: 0 });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(elName, { opacity: 1, duration: 1.5, ease: "expo.inOut" });
    return tl;
  };
}
