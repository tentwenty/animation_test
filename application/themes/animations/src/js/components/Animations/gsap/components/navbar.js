import NavSliderAnimation from "../timelines/types/nav-slider";

export default class NavbarSlider {
  constructor(params) {
    this.navSliderAnimation = new NavSliderAnimation();
    
  }

  open = () => {
    this.navSliderAnimation.play({ toggle: true });
  };
  close = () => {
    this.navSliderAnimation.play({ toggle: false });
  };
}
                                                                 