import gsap from "gsap";

export default class MasterTimeline {
  constructor() {
    this.gsap = gsap;
  }

  getMasterTimeline = (options) => {
    let timeline = new gsap.timeline(options);

    return timeline;
  };

  getTimeline = (options) => {
    let timeline = new gsap.timeline(options);
    timeline.eventCallback("onComplete", function () {
      let targets = this._recent._targets;
      if (targets.length) {
        targets.forEach((v, k) => {
          let $el = $(v);
          $el.addClass("animated");
        });
      }
    });
    return timeline;
  };
}
