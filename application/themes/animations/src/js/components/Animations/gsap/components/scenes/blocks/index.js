import QuoteBlock from "./quoteBlock";

export default class Blocks {
  constructor() {
    this.bindAll();
  }

  bindAll() {
    new QuoteBlock();
  }
}
