import MasterTimeline from "../masterTimeline";

export default class CrossSlide {
  constructor(params) {
    this.direction = params.direction;
    this.masterTimeLine = new MasterTimeline();
    this.mtl = this.masterTimeLine.getMasterTimeline({});
    this.gsap = this.masterTimeLine.gsap;
  }

  play = () => {
    if (this.direction.toLowerCase() === "right") {
      this.mtl.add(this.slideRight());
    } else if (this.direction.toLowerCase() === "left") {
      this.mtl.add(this.slideLeft());
    } else {
      this.mtl.add(this.slideRight());
    }

    return this.mtl;
  };

  slideLeft = () => {
    let elName = ".cross-slide--left";
    this.gsap.set(elName, { x: "-10%", y: "100%", opacity: 0 });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(elName, {
      x: "0%",
      y: "0%",
      opacity: 1,
      duration: 1,
      ease: "expo.out",
    });
    return tl;
  };

  slideRight = () => {
    let elName = ".cross-slide--right";
    this.gsap.set(elName, { x: "10%", y: "100%", opacity: 0 });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(elName, {
      x: "0%",
      y: "0%",
      opacity: 1,
      duration: 3,
      ease: "expo.out",
    });
    return tl;
  };
}
