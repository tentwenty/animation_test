import MasterTimeline from "../masterTimeline";

export default class LetterFade {
  constructor(params) {
    this.masterTimeLine = new MasterTimeline();
    this.mtl = this.masterTimeLine.getMasterTimeline({});
    this.gsap = this.masterTimeLine.gsap;
  }

  play = () => {
    this.mtl.add(this.fadeIn());
    return this.mtl;
  };

  fadeIn = () => {
    let elName = ".letter-fade-up span";
    $(elName).css({ position: "relative" });
    this.gsap.set(elName, { opacity: 0, top: "-100px" });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(elName, {
      opacity: 1,
      top: "0",
      duration: 1,
      stagger: 0.05,
      ease: "expo.inOut",
    });
    return tl;
  };
}
