import Blocks from "./blocks";
import CommonScenes from "./base/common";

export default class Scenes {
  constructor() {
    this.bindAll();
  }

  bindAll() {
    new CommonScenes();
    new Blocks();
  }
}
