import MasterTimeline from "../masterTimeline";

export default class SlideIn {
  constructor(params) {
    this.direction = params.direction;
    this.masterTimeLine = new MasterTimeline();
    this.mtl = this.masterTimeLine.getMasterTimeline({});
    this.gsap = this.masterTimeLine.gsap;
  }

  play = () => {
    if (this.direction.toLowerCase() === "right") {
      this.mtl.add(this.slideRight());
    } else if (this.direction.toLowerCase() === "left") {
      this.mtl.add(this.slideLeft());
    } else if (this.direction.toLowerCase() === "top") {
      this.mtl.add(this.slideTop());
    } else if (this.direction.toLowerCase() === "bottom") {
      this.mtl.add(this.slideBottom());
    } else {
      this.mtl.add(this.slideRight());
    }

    return this.mtl;
  };

  slideLeft = () => {
    this.gsap.set(".slide--left", { x: "200%", opacity: 0 });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(".slide--left", {
      x: "0%",
      opacity: 1,
      duration: 1,
      ease: "expo.inOut",
    });
    return tl;
  };

  slideRight = () => {
    this.gsap.set(".slide--right", { x: "-200%", opacity: 0 });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(".slide--right", {
      x: "0%",
      opacity: 1,
      duration: 1,
      ease: "expo.inOut",
    });
    return tl;
  };

  slideTop = () => {
    this.gsap.set(".slide--top", { y: "-50%", opacity: 0 });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(".slide--top", {
      y: "0%",
      opacity: 1,
      duration: 1,
      ease: "expo.inOut",
    });
    return tl;
  };

  slideBottom = () => {
    this.gsap.set(".slide--bottom", { y: "100%", opacity: 0 });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(".slide--bottom", {
      y: "0%",
      opacity: 1,
      duration: 1,
      ease: "expo.inOut",
    });
    return tl;
  };
}
