import MasterTimeline from "../masterTimeline";

export default class LineUp {
  constructor(params) {
    this.masterTimeLine = new MasterTimeline();
    this.mtl = this.masterTimeLine.getMasterTimeline({});
    this.gsap = this.masterTimeLine.gsap;
  }

  play = () => {
    this.mtl.add(this.bTranslateUp());
    return this.mtl;
  };

  bTranslateUp = () => {
    let elName = ".beam-wrap";
    this.gsap.set(elName, { y: "115%" });
    let tl = this.masterTimeLine.getTimeline({ repeat: -1, repeatDelay: 2 });
    tl.to(elName, { y: "-5%", duration: 2, ease: "expo.inOut" });
    return tl;
  };
}
