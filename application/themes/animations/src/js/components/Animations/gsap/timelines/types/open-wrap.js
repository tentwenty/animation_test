import MasterTimeline from "../masterTimeline";

export default class OpenWrap {
  constructor(params) {
    this.masterTimeLine = new MasterTimeline();
    this.mtl = this.masterTimeLine.getMasterTimeline({});
    this.gsap = this.masterTimeLine.gsap;
  }

  play = () => {
    this.mtl.add(this.openWrap());
    return this.mtl;
  };

  openWrap = () => {
    let elName = ".open-wrap";
    this.gsap.set(elName, { height: "0%" });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(elName, { height: "100%", duration: 1, ease: "circ.inOut" });
    return tl;
  };
}
