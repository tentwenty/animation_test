import MasterTimeline from "../masterTimeline";

export default class StaggerText {
  constructor(params) {
    this.masterTimeLine = new MasterTimeline();
    this.mtl = this.masterTimeLine.getMasterTimeline({});
    this.gsap = this.masterTimeLine.gsap;
  }

  play = () => {
    this.mtl.add(this.staggerText());
    return this.mtl;
  };

  staggerText = () => {
    let elName = ".stagger--text p";
    this.gsap.set(elName, { opacity: 0, y: "100%" });
    let tl = this.masterTimeLine.getTimeline();
    tl.to(elName, {
      opacity: 1,
      y: "0%",
      duration: 1,
      stagger: 0.15,
      ease: "Power2.easeOut",
    });

    return tl;
  };
}
