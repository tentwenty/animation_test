import SceneBuilder from "./sceneBuilder";

export default class AutoBuilder {
  constructor() {
    this.init();
  }

  init() {
    new SceneBuilder({
      elName: "auto-build--",
    });
  }
}
