import Scenes from "./components/scenes";
import AutoBuilder from "./builder";
export default class GsapAnimations {
  constructor() {
    this.init();
  }

  init = () => {
    new AutoBuilder();
    new Scenes();
  };
}
