import LineUp from "../../../timelines/types/line-up";
import SlideIn from "../../../timelines/types/slide-in";
import FadeIn from "../../../timelines/types/fade-in";
import CrossSlide from "../../../timelines/types/cross-slide";
import OpenWrap from "../../../timelines/types/open-wrap";
import LetterFade from "../../../timelines/types/letter-fade";

export default class CommonScenes {
  constructor() {
    this.bindScenes();
  }

  bindScenes() {
    let letterFade = new LetterFade();
    letterFade.play();

    let lineUp = new LineUp();
    lineUp.play();

    let slideInRight = new SlideIn({
      direction: "right",
    });
    slideInRight.play();
    let slideInLeft = new SlideIn({
      direction: "left",
    });
    slideInLeft.play();

    let slideInTop = new SlideIn({
      direction: "top",
    });
    slideInTop.play();

    let slideInBottom = new SlideIn({
      direction: "bottom",
    });
    slideInBottom.play();

    let crossSlideRight = new CrossSlide({
      direction: "right",
    });
    crossSlideRight.play();

    let fadeIn = new FadeIn();
    fadeIn.play();

    let openWrap = new OpenWrap();
    openWrap.play();
  }
}
