import StaggerText from "../../../timelines/types/stagger-text";
import IsInView from "../../../../../../Helpers/IsInView";

export default class QuoteBlock {
  constructor(params) {
    this.isInView = new IsInView();

    this.init();
  }

  init = () => {
    this.bindEvents();
  };

  bindEvents = () => {
    this.staggerText();
  };

  staggerText = () => {
    this.isInView.observe(
      document.querySelectorAll(".stagger--text"),
      function (callback) {
        if (callback) {
          let staggerText = new StaggerText();
          staggerText.play();
        }
      }
    );
  };
}
