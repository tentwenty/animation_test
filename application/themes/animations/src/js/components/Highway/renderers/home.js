import Highway from '@dogstudio/highway';

export default class HomeRender extends Highway.Renderer {
  // Built-in methods
  onEnter() {   }
  onLeave() {   }
  onEnterCompleted() {  }
  onLeaveCompleted() {   }
}