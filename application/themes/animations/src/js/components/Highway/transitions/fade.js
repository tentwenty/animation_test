import Highway from "@dogstudio/highway";
import gsap from "gsap";

export default class Fade extends Highway.Transition {
  in({ from, to, done }) {
    const tl = new gsap.timeline();
    
    tl.fromTo(to, 0.5, { left: "-100%", top: "50%" }, { left: "0" }).fromTo(
      to,
      0.5,
      { height: "2%" },
      {
        height: "100%",
        top: "10%",
        onComplete: function () {
          $(from).remove();
          done();
        },
      }
    );
  }

  out({ from, done }) {
    done();
  }
}
