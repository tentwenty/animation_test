import Highway from "@dogstudio/highway";
import Fade from "./transitions/fade";
import HomeRender from "./renderers/home";
export default class HighwayJs {
  constructor({ scroll }) {
    this.scroll = scroll.scroll;
    this.init();
  }

  init() {
    const highway = new Highway.Core({
      renderers: {},
      transitions: {
        default: Fade,
      },
    });

    highway.on("NAVIGATE_END", ({ to, from, trigger, location }) => {
      if (this.scroll) {
        this.scroll.update();
      }

      var inlineScript = $('html').find("script");
      inlineScript.each(function (i, elm) {
        eval(elm.innerHTML);
      });
    });
  }
}
