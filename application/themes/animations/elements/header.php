<?php defined('C5_EXECUTE') or die("Access Denied.");

$site = Config::get('concrete.site');
$themePath = $this->getThemePath();

?>

<?php $this->inc('elements/header_top.php'); ?>
<div class="mobile-nav">
</div>
<aside>
    <div class='sidenav-wrapper __border-right'>
        <span class="beam" data-scroll data-scroll-repeat>
            <span class="beam-wrap -right"></span>
        </span>
        <div class='brand-logo slide--right'>
            <p>logo here</p>
        </div>
        <div id="hamburger-trigger" class='hamburger-wrapper slide--right' data-nav-open>
            <span class='hamburger-line'></span>
        </div>
        <div class='slogan-wrapper slide--bottom'>
            <p>Digital <br> Agency Leader</p>
        </div>
    </div>
    <div class="open-navSlide-container">
        <div class="open-navSlide-wrapper">
            <div class="open-navSlide-item open-navSlide"></div>
        </div>
        <div class="open-navSlide-wrapper --right">
            <div class="open-navSlide-item open-navSlide"></div>
        </div>
    </div>
</aside>
<header>
</header>