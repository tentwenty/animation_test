<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if ($image) { ?>


    <section data-scroll-section class='image-block-wrapper'>
        <div class='bg-img lozad' data-scroll data-scroll-speed='-3' data-src="<?php echo $image->getURL(); ?>" alt="<?php echo $image->getTitle(); ?>"></div>
    </section>

<?php  } ?>