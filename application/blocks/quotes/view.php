<?php defined("C5_EXECUTE") or die("Access Denied."); ?>
<?php if (isset($decription) && trim($decription) != "") { ?>

    <section data-scroll-section class="t-layout">

        <div data-scroll class="t-layout_item t-padding">

            <div class="t-text-quote stagger--text">
                <?php echo $decription; ?>
            </div>
        </div>

    </section>

<?php  } ?>