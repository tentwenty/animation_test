<?php  namespace Application\Block\Quotes;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Concrete\Core\Editor\LinkAbstractor;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('decription');
    protected $btExportFileColumns = array();
    protected $btTable = 'btQuotes';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Quotes");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->decription;
        return implode(" ", $content);
    }

    public function view()
    {
        $this->set('decription', LinkAbstractor::translateFrom($this->decription));
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
        
        $this->set('decription', LinkAbstractor::translateFromEditMode($this->decription));
    }

    protected function addEdit()
    {
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function save($args)
    {
        $args['decription'] = LinkAbstractor::translateTo($args['decription']);
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("decription", $this->btFieldsRequired) && (trim($args["decription"]) == "")) {
            $e->add(t("The %s field is required.", t("description")));
        }
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}