<?php  defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php  echo $form->label('description_1', t("description")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('description_1', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make('editor')->outputBlockEditModeEditor($view->field('description_1'), $description_1); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('ctaText', t("cta_text")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('ctaText', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->text($view->field('ctaText'), $ctaText, array (
  'maxlength' => 255,
)); ?>
</div>

<div class="form-group">
    <?php  echo $form->label('smoothScroll', t("smooth-scroll")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('smoothScroll', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo $form->select($view->field('smoothScroll'), $smoothScroll_options, $smoothScroll, array()); ?>
</div>