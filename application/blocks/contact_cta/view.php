<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<?php if (trim($smoothScroll) != "") { ?>

    <?php switch ($smoothScroll) {
        case "yes": ?>
            <section data-scroll-section class='strategy-wrapper t-layout'>

                <div data-scroll class='t-layout_item t-layout_item--two-third strat-item __border-right_n'>
                    <span class="beam">
                        <span class="beam-wrap -right"></span>
                    </span>
                    <p data-canimate-defaults="" data-canimate-="expo.inOut" class='t-text-blue t-padding-x t-padding-small-y slide--bottom banner-breadcrumb'>Service</p>
                    <h1 class='t-padding-x cross-slide--right banner-title'>STRATEGY</h1>
                </div>
                <div data-scroll class='t-layout_item t-layout_item--one-third '>

                    <div class="t-padding __border-top_n">
                        <?php if (isset($description_1) && trim($description_1) != "") { ?>
                            <div class='t-text-p slide--bottom banner-description letter-fade-up __split-text h3 '>
                                <?php echo $description_1; ?>
                            </div>
                        <?php } ?>
                        <?php if (isset($ctaText) && trim($ctaText) != "") { ?>

                            <a href="#" class="cta-button  slide--bottom">
                                <span class="cta-button_label_wrap">
                                    <span class="cta-button_label">
                                        <?php echo h($ctaText); ?>
                                    </span>
                                </span>

                            </a>
                        <?php } ?>
                    </div>
                </div>
                <div data-scroll class='t-layout_item empty-block'>
                    <div class='filler-block open-wrap'></div>
                </div>
            </section>
            <?php break;
        case "no":
            // ENTER MARKUP HERE FOR FIELD "smooth-scroll" : CHOICE "no"
            break;
    } ?><?php  } ?>