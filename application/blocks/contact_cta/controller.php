<?php  namespace Application\Block\ContactCta;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use Concrete\Core\Editor\LinkAbstractor;

class Controller extends BlockController
{
    public $helpers = array('form');
    public $btFieldsRequired = array('description_1', 'ctaText');
    protected $btExportFileColumns = array();
    protected $btTable = 'btContactCta';
    protected $btInterfaceWidth = 400;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Contact");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->description_1;
        $content[] = $this->ctaText;
        return implode(" ", $content);
    }

    public function view()
    {
        $this->set('description_1', LinkAbstractor::translateFrom($this->description_1));
        $smoothScroll_options = array(
            '' => "-- " . t("None") . " --",
            'yes' => "yes",
            'no' => "no"
        );
        $this->set("smoothScroll_options", $smoothScroll_options);
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
        
        $this->set('description_1', LinkAbstractor::translateFromEditMode($this->description_1));
    }

    protected function addEdit()
    {
        $this->set("smoothScroll_options", array(
                '' => "-- " . t("None") . " --",
                'yes' => "yes",
                'no' => "no"
            )
        );
        $this->requireAsset('redactor');
        $this->requireAsset('core/file-manager');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
    }

    public function save($args)
    {
        $args['description_1'] = LinkAbstractor::translateTo($args['description_1']);
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("description_1", $this->btFieldsRequired) && (trim($args["description_1"]) == "")) {
            $e->add(t("The %s field is required.", t("description")));
        }
        if (in_array("ctaText", $this->btFieldsRequired) && (trim($args["ctaText"]) == "")) {
            $e->add(t("The %s field is required.", t("cta_text")));
        }
        if ((in_array("smoothScroll", $this->btFieldsRequired) && (!isset($args["smoothScroll"]) || trim($args["smoothScroll"]) == "")) || (isset($args["smoothScroll"]) && trim($args["smoothScroll"]) != "" && !in_array($args["smoothScroll"], array("yes", "no")))) {
            $e->add(t("The %s field has an invalid value.", t("smooth-scroll")));
        }
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}